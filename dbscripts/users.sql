CREATE TABLE `users` (
  `id` int(11)                          NOT NULL AUTO_INCREMENT,
  `username` varchar(255)               NOT NULL,
  `password` varchar(255)               DEFAULT NULL,
  `firstName` varchar(255)              DEFAULT NULL,
  `lastName` varchar(255)               DEFAULT NULL,
  `email` varchar(255)                  NOT NULL,
  `addressLine1` varchar(255)           DEFAULT NULL,
  `addressLine2` varchar(255)           DEFAULT NULL,
  `city` varchar(255)                   DEFAULT NULL,
  `postcode` varchar(255)               DEFAULT NULL,
  `phone` varchar(255)                  DEFAULT NULL,
  `reset_password_token` varchar(255)   DEFAULT NULL,
  `remember_created_at` datetime        DEFAULT NULL,
  `sign_in_count` int(11)               DEFAULT NULL,
  `current_sign_in_at` datetime         DEFAULT NULL,
  `last_sign_in_at` datetime            DEFAULT NULL,
  `current_sign_in_ip` varchar(255)     DEFAULT NULL,
  `last_sign_in_ip` varchar(255)        DEFAULT NULL,
  `isAdmin` tinyint(1)                  DEFAULT NULL,
  `reset_password_sent_at` datetime     DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `authentication_provider` (
  `id` int(11)                  NOT NULL AUTO_INCREMENT,
  `providerId` varchar(255)     DEFAULT NULL,
  `userId` int(11)              NOT NULL,
  `providerType` varchar(255)   DEFAULT NULL,
  `displayName` varchar(255)    DEFAULT NULL,
  `profile_photo` varchar(1000),
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `authentication_provider_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
);
