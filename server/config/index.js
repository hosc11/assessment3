'use strict';

var ENV = process.NODE_ENV || 'development';

module.exports = require('./' + ENV + '.js') || {};