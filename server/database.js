// Loads sequelize ORM
var Sequelize = require("sequelize");
var config = require("./config");

// DBs, MODELS, and ASSOCIATIONS ---------------------------------------------------------------------------------------
// Creates a MySQL connection
var connection = new Sequelize(config.mysql, 
    {
        logging: console.log,
        dialect: 'mysql',   
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var Employee = require('./models/employees')(connection, Sequelize);

var Department = require('./models/departments')(connection, Sequelize);
var DeptEmp = require('./models/deptemp')(connection, Sequelize);

Employee.hasMany(DeptEmp, {foreignKey: 'emp_no'})
DeptEmp.belongsTo(Department, {foreignKey: 'dept_no'}); 

module.exports = {
    Employee: Employee,
    Department: Department,
    DeptEmp: DeptEmp,
    connection: connection,
};